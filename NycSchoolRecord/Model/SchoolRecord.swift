//
//  SchoolRecord.swift
//  NycSchoolRecord
//
//  Created by Naga Mulpuri on 3/15/23.
//

import Foundation

struct SchoolRecord: Decodable {
    let schoolName: String
    let testTakersCount: String
    let readingScore: String
    let writingScore: String
    let mathScore: String
    
    enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
        case testTakersCount = "num_of_sat_test_takers"
        case readingScore = "sat_critical_reading_avg_score"
        case writingScore = "sat_writing_avg_score"
        case mathScore = "sat_math_avg_score"
    }
}
