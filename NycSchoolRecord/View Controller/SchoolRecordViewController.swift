//
//  ViewController.swift
//  NycSchoolRecord
//
//  Created by Naga Mulpuri on 3/15/23.
//

import UIKit

class SchoolRecordViewController: UITableViewController {
    
    let viewModel = SchoolRecordViewModel()
    private let fetchURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.systemTeal]
        viewModel.fetchData(URL: fetchURL) { error in
            if let error = error {
                //Show alert view based on error
                print(error)
            }else {
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.schoolRecords.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolRecordCell") as! SchoolRecordCell
        /* We can also add some search filter to search the school names */
        let schoolRecord: SchoolRecord
        schoolRecord = viewModel.schoolRecords[indexPath.row]
        cell.schoolName?.text = schoolRecord.schoolName
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       if let vc = storyboard?.instantiateViewController(withIdentifier: "AverageScoresViewController") as? AverageScoresViewController {
           let schoolRecord: SchoolRecord
           schoolRecord = viewModel.schoolRecords[indexPath.row]
           vc.schoolRecord = schoolRecord
           self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

