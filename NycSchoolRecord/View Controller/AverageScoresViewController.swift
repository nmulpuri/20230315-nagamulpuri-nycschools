//
//  averageScoresViewController.swift
//  NycSchoolRecord
//
//  Created by Naga Mulpuri on 3/15/23.
//

import Foundation
import UIKit

class AverageScoresViewController: UIViewController {
    
    @IBOutlet weak var writingScoreLabel: UILabel!
    @IBOutlet weak var testersCountLabel: UILabel!
    @IBOutlet weak var mathScoreLabel: UILabel!
    @IBOutlet weak var readingScoreLabel: UILabel!
    @IBOutlet weak var schoolNameLabel: UILabel!
    
    var schoolRecord: SchoolRecord?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /* If I have more time I would have displayed the more information of schools which is provided in this json "https://data.cityofnewyork.us/resource/s3k6-pzi2.json".
         1. We can add the address and phone numbers of the school.
         2. We can integrate the mapkit and add a pointer of the school*/
        
        title = "Average Score"
        navigationController?.navigationBar.tintColor = UIColor.systemTeal
        
        testersCountLabel.text = schoolRecord?.testTakersCount
        writingScoreLabel.text = schoolRecord?.writingScore
        readingScoreLabel.text = schoolRecord?.readingScore
        mathScoreLabel.text = schoolRecord?.mathScore
        schoolNameLabel.text = schoolRecord?.schoolName
    
    }
}
