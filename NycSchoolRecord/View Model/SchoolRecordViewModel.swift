//
//  SchoolRecordViewModel.swift
//  NycSchoolRecord
//
//  Created by Naga Mulpuri on 3/15/23.
//

import Foundation

enum DataError: Error {
    case invalidURL
    case decodingError
    case emptyData
}

class SchoolRecordViewModel {
    var schoolRecords: [SchoolRecord] = []
    
    typealias SchoolRecordResponse = ([SchoolRecord], Error?)
    
    func fetchData(URL url:String, completion: @escaping(Error?) -> Void) {
        guard let url = URL(string: url) else {
            schoolRecords = []
            completion(DataError.invalidURL)
            return
        }
        
        let dataTask = URLSession.shared.dataTask(with: url) { [weak self] data, response, error in
            self?.schoolRecords = []
            if let error = error {
                completion(error)
            }else if data == nil {
                completion(DataError.emptyData)
            }else {
                do {
                    let fetchedData = try JSONDecoder().decode([SchoolRecord].self, from: data!)
                    self?.schoolRecords = fetchedData
                    completion(nil)
                    
                }catch {
                    completion(DataError.decodingError)
                }
            }
        }
        dataTask.resume()
    }
}
